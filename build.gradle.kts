import de.undercouch.gradle.tasks.download.Download
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin on the JVM.
    id("org.jetbrains.kotlin.jvm") version "1.3.21"

    // Apply the application plugin to add support for building a CLI application.
    application

    // Thi plugin is needed to download arbitrary files
    id("de.undercouch.download") version "3.4.3"
}

group = "com.gitlab.semanticweb1819"
version = "0.2.1"

val ontologyVersion = "v0.3"

repositories {
    jcenter()
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // https://mvnrepository.com/artifact/ch.qos.logback/logback-classic
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("io.github.microutils:kotlin-logging:1.6.24")

    implementation("com.diffplug.spotless:spotless-eclipse-cdt:9.7.0")

    // https://mvnrepository.com/artifact/org.apache.jena/apache-jena-libs
    implementation("org.apache.jena:apache-jena-libs:3.11.0")

    // Use the Kotlin test library.
    // testImplementation("org.jetbrains.kotlin:kotlin-test")
    // Use the Kotlin JUnit integration.
    // testImplementation("org.jetbrains.kotlin:kotlin-test-junit")

    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.2")
}

application {
    // Define the main class for the application.
    mainClassName = "com.gitlab.semanticweb1819.AppKt"
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

val ontologyFileName = "COntology.owl"
val ontologyResourceFolder: File = sourceSets.main.get().resources.srcDirs.first() // resources directory
val cOntologyResourceDestFile = File(ontologyResourceFolder, ontologyFileName)

val downloadOntologyTask = tasks.register<Download>("downloadOntology") {
    src("https://gitlab.com/semantic-web-1819/c-owl-ontology/raw/$ontologyVersion/$ontologyFileName")
    dest(cOntologyResourceDestFile)
    onlyIfModified(true)
}

val assemble by tasks.existing { dependsOn(downloadOntologyTask) }
val test by tasks.existing { dependsOn(downloadOntologyTask) }

/* Added task to clean downloaded ontology as it's not part of that project, but a dependency */
val clean by tasks.existing {
    doLast {
        println("Deleting: $cOntologyResourceDestFile")
        if (delete(cOntologyResourceDestFile)) println("Done.") else println("No such file, skipping.")
    }
}