package com.gitlab.semanticweb1819.rdf

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource

object Syllabus {

    const val NAMESPACE = "http://www.semanticweb.org/diego/ontologies/2019/SyllabusV2Merge#"

    private infix fun String.resource(resourceName: String): Resource = COntology.model.getResource(this + resourceName)
    private infix fun String.property(propertyName: String): Property = COntology.model.getProperty(this + propertyName)


    val C_SWITCH = NAMESPACE resource "C_Switch"
    val C_ARRAY = NAMESPACE resource "C_Array"
    val C_ENUM = NAMESPACE resource "C_Enum"
    val C_IF = NAMESPACE resource "C_If"
    val C_FOR = NAMESPACE resource "C_For"
    val C_WHILE = NAMESPACE resource "C_While"
    val C_DO = NAMESPACE resource "C_Do"
    val C_FUNCTION = NAMESPACE resource "C_Function"
    val C_LIBRARY = NAMESPACE resource "C_Library"
    val C_STRUCT = NAMESPACE resource "C_Struct"
    val C_POINTER = NAMESPACE resource "C_Pointer"


    val SOURCE = NAMESPACE resource "Source"

    val CONCEPT_PROPERTY = NAMESPACE property "hasConcept"

}