package com.gitlab.semanticweb1819

import org.apache.jena.rdf.model.ModelFactory
import java.io.File
import java.nio.file.Files
import kotlin.system.exitProcess

object Merger {

    val defaultOntologyURL: String = "D:\\Documenti\\Dropbox\\Uni\\WS - Web Semantico\\Consegna 2-3\\Merge\\c-source-annotator-master\\Ontology\\Syllabus.owl"
    val outputFileName: String = "output.owl"

    fun merge(path: String) {
        val model = ModelFactory.createDefaultModel()
        model.read(defaultOntologyURL)
        File(path).walk().filter { file -> file.extension == "rdf" }.forEach {
           model.read(it.absolutePath)
        }

        val outputStream = try {
            File(path + "/" + outputFileName).outputStream()
        } catch (e: Exception) {
            exitProcess(1)
        }

        model.write(outputStream)
    }
}