
# *Proof of Concept* per l'annotazione semantica di sorgenti C

## Requisiti di sistema

- Java 11+ 

## Guida di utilizzo

 - Scaricare il repository e posizionarsi con un terminale nella "root directory"
 - Eseguire il comando `gradlew run --arg="PathToInputFile PathToOutputFile"`